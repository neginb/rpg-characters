﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Armor : Item
    {
        public PrimaryAttributes Attributes { get; set; }
        public CharArmorsType ArmorsType { get; set; }

        //Using enum to give the available types to the Armors
        public enum CharArmorsType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }
    }
}
