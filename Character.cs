﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public abstract class Character
    {
        public string Name { get; set; }

        //Giving 1 as a defualt value for the level to each character
        public int Level { get; set; } = 1;
        public PrimaryAttributes PrimaryAttributes { get; set; }
        public double TotalPrimaryAttributes { get; set; }


        //A dictionary with each item and Slot for equipped items as weapons and armors
        public Dictionary<Slot, Item> Equipment = new Dictionary<Slot, Item>();

        /// <summary>
        /// This method increase the characters' primary attributes at different rates as the character gains levels.
        /// </summary>
        public abstract void LevelIncreasment();

        /// <summary>
        /// This method check the condition for gaining of a weapon
        /// If the chosen weapon is equal to one of the allowed types,the character level is less or equal to the required level and The slot is of type Weapon, then equip the weapon
        /// Add the equipped weapon to the Equipment dictionary
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>"New weapon equipped!", if a weapon is equipped</returns>
        /// <returns>throw an InvalidWeaponException if the equiption fails</returns>
        public abstract string EquipWeapon(Weapons weapons);

        /// <summary>
        ///  This method check the condition for gaining of an armor
        ///  If the chosen armor is equal to one of the allowed types,the character level is less or equal to the required level and The slot is not of type weapon, then equip the armor
        ///  Add the equipped armor to the Equipment dictionary
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>"New armor equipped!"</returns>
        /// <returns>throw an InvalidArmorException if the equiption fails</returns>
        public abstract string EquipArmor(Armor armor);

        /// <summary>
        /// This method calculates the total attributes of each character
        /// This method add all equipped armors' primary attribute, which increas the damage of that character by one percent, to the characters' primary attribute from the current level
        /// </summary>
        /// <returns>Sum of total primary attributes</returns>
        public abstract double TotalAttributes();

        /// <summary>
        /// This method calculates the damage of each character based on the result of TotalAttributes method
        /// If a weapon is equipped then include the weapons DPS in the calculation
        /// If no weapon is equipped, calculates the damage of character with DPS = 1
        /// </summary>
        /// <returns>the calculated damage of the character with 3 decimals</returns>
        public virtual double Damage()
        {
            double damage = 0;

            if (Equipment.ContainsKey(Slot.Weapon))
            {
                Weapons weapons = (Weapons)Equipment[Slot.Weapon];
                damage += weapons.CalculateDps() * (1 + (TotalAttributes() / 100));
              return Math.Round(damage, 3);
            }
            damage += 1 * (1 + (TotalAttributes() / 100));
            return Math.Round(damage, 3);
        }

        /// <summary>
        /// This method go through Equipment dictionary and add all armors' attributes to the characters attributes from the current level
        /// </summary>
        /// <returns>the characters' name, current level, Total Primary attributes and the damage of the character</returns>
        public string Display()
        {
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor gainedArmor = (Armor)Equipment[item.Key];
                    PrimaryAttributes.Strength += gainedArmor.Attributes.Strength;
                    PrimaryAttributes.Dexterity += gainedArmor.Attributes.Dexterity;
                    PrimaryAttributes.Intelligence += gainedArmor.Attributes.Intelligence;
                }
            }
            StringBuilder display = new StringBuilder();
            display.Append("The name of your character is " + Name + " , and your level is " + Level + "\n");
            display.Append("Your strength is: " + PrimaryAttributes.Strength + "\n");
            display.Append("Your dexterity is: " + PrimaryAttributes.Dexterity + "\n");
            display.Append("Your intellignce is: " + PrimaryAttributes.Intelligence + "\n");
            display.Append("And last but not list, the damage of your character is: " + Damage() + "\n");
            return display.ToString();
        }
    }
}
