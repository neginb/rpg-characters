# RPG-Characters

It is a .NET Core Console Application. This project is the logic behind the RPG-Characters game. It has four character classes that can equip armors or weapons. This project covers the character creation, leveling up, and calculation of each character's damage. This project contains a RPG-CharacterTest project that tests the functionality of this code.

## Getting started
```
Open Visual Studio and choose Clone a repository.
Use `https://gitlab.com/neginb/rpg-characters` for the "Repository location".
Choose a local directory for the project.
Clone and open the solution in Visual Studio.
Run.

```

# Prerequisites
- .NET Framework
- Visual Studio 2019 OR Visual Studio Code



## Test
```
To test the project, right click on the RPG-CharacterTest project in the solution and click on "Run Tests".
You can also write your own tests in the Program class.

```
## Contributing

No contributions allowed.

## Authors

Negin Bakhtiarirad (@neginb)

