﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Weapons : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; }

        public CharWeaponsType WeaponsType { get; set; }

        //Compose a weapon with its type.
        public enum CharWeaponsType
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        /// <summary>
        /// Calculates the dps value of each equipped weapon, by multiply the Damage value to the AttackSpeed value of each weapon
        /// </summary>
        /// <returns>calculated dps value</returns>
        public double CalculateDps()
        {
            double dps = WeaponAttributes.Damage * WeaponAttributes.AttackSpeed;
            return dps;
        }
    }
}
