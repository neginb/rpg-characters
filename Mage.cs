﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Mage : Character
    {
        //Constructor - Making a new Mage character
        public Mage()
        {
            //Get access to priymay attributes class and set the values
            PrimaryAttributes = new PrimaryAttributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponsType == Weapons.CharWeaponsType.Wand && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel || weapon.WeaponsType == Weapons.CharWeaponsType.Staff && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel)
                {
                    Equipment.Add(weapon.ItemSlot, weapon);
                    return "New weapon equipped!";
            }
            else
                {
                    throw new InvalidWeaponException();
                }
        }

        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorsType == Armor.CharArmorsType.Cloth && armor.ItemSlot != Slot.Weapon && Level >= armor.RequiredLevel)
                {
                    Equipment.Add(armor.ItemSlot, armor);
                    return "New armor equipped!";
            }
            else
                {
                    throw new InvalidArmorException();
                }
        }

        //Primary attribute Intelligance is the Mages' primary attribute which increase the damage of this character by one procent
        public override double TotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor gainedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += gainedArmor.Attributes.Intelligence;
                }
            }
            TotalPrimaryAttributes += PrimaryAttributes.Intelligence;
            return TotalPrimaryAttributes;
        }

        public override void LevelIncreasment()
        {
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 1;
            PrimaryAttributes.Intelligence += 5;
            Level += 1;
        }
    };
}
