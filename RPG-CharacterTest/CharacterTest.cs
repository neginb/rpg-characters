﻿using System;
using RPG_Characters;
using Xunit;

namespace RPG_CharacterTest
{
    public class CharacterTest
    {
        //////////////////////////////////////////////Checking the Mage Character///////////////////////////////////////////////////////////

        [Fact]
        public void ConstructorMage_CreatingNewMageCharacter_ShouldReturnStrengthOneDexterityOneIntelliganceEightLevelOne()
        {
            //Arrange
            int[] expected = new int[] { 1, 1, 8, 1 };

            //Act
            Mage mage1 = new Mage();

            //Assert
            Assert.Equal(expected, new int[] { mage1.PrimaryAttributes.Strength, mage1.PrimaryAttributes.Dexterity, mage1.PrimaryAttributes.Intelligence, mage1.Level });
        }

        [Fact]
        public void LevelIncreasment_CreatingNewMageCharacterWithLevelOne_ShouldReturnStrengthTwoDexterityTwoIntelliganceThirteenLevelTwo()
        {
            //Arrange
            Mage mage2= new Mage();
            int[] expected = new int[] { 2, 2, 13, 2 };

            //Act
            mage2.LevelIncreasment();

            //Assert
            Assert.Equal(expected, new int[] { mage2.PrimaryAttributes.Strength, mage2.PrimaryAttributes.Dexterity, mage2.PrimaryAttributes.Intelligence, mage2.Level });
        }

        //////////////////////////////////////////////Checking the Warrior Character///////////////////////////////////////////////////////////

        [Fact]
        public void ConstructorWarrior_CreatingNewWarriorCharacter_ShouldReturnStrengthFiveDexterityTowIntelliganceOneLevelOne() 
        {
            //Arrange
            int[] expected = new int[] { 5, 2, 1, 1 };

            //Act
            Warrior warrior1 = new Warrior();

            //Assert
            Assert.Equal(expected, new int[] { warrior1.PrimaryAttributes.Strength, warrior1.PrimaryAttributes.Dexterity, warrior1.PrimaryAttributes.Intelligence, warrior1.Level });
        }

        [Fact]
        public void LevelIncreasment_CreatingNewWarriorCharacterWithLevelOne_ShouldReturnStrengthEightDexterityFourIntelliganceTwoLevelTwo()
        {
            //Arrange
            Warrior warrior2 = new Warrior();
            int[] expected = new int[] { 8, 4, 2, 2 };

            //Act
            warrior2.LevelIncreasment();

            //Assert
            Assert.Equal(expected, new int[] { warrior2.PrimaryAttributes.Strength, warrior2.PrimaryAttributes.Dexterity, warrior2.PrimaryAttributes.Intelligence, warrior2.Level });
        }

        //////////////////////////////////////////////Checking the Ranger Character///////////////////////////////////////////////////////////

        [Fact]
        public void ConstructorRanger_CreatingNewRangerCharacter_ShouldReturnStrengthOneDexteritySevenIntelliganceOneLevelOne()
        {
            //Arrange
            int[] expected = new int[] { 1, 7, 1, 1 };

            //Act
            Ranger ranger1 = new Ranger();


            //Assert
            Assert.Equal(expected, new int[] { ranger1.PrimaryAttributes.Strength, ranger1.PrimaryAttributes.Dexterity, ranger1.PrimaryAttributes.Intelligence, ranger1.Level });
        }

        [Fact]
        public void LevelIncreasment_CreatingNewRangerCharacterWithLevelOne_ShouldReturnStrengthTwoDexterityTwelveIntelliganceTwoLevelTwo()
        {
            //Arrange
            Ranger ranger2 = new Ranger();
            int[] expected = new int[] { 2, 12, 2, 2 };

            //Act
            ranger2.LevelIncreasment();

            //Assert
            Assert.Equal(expected, new int[] { ranger2.PrimaryAttributes.Strength, ranger2.PrimaryAttributes.Dexterity, ranger2.PrimaryAttributes.Intelligence, ranger2.Level });
        }

        //////////////////////////////////////////////Checking the Rogue Character///////////////////////////////////////////////////////////

        [Fact]
        public void ConstructorRogue_CreatingNewRogueCharacter_ShouldReturnStrengthTwoDexteritySexIntelliganceOneLevelOne()
        {
            //Arrange
            int[] expected = new int[] { 2, 6, 1, 1 };

            //Act
            Rogue rogue1 = new Rogue();


            //Assert
            Assert.Equal(expected, new int[] { rogue1.PrimaryAttributes.Strength, rogue1.PrimaryAttributes.Dexterity, rogue1.PrimaryAttributes.Intelligence, rogue1.Level });
        }

        [Fact]
        public void LevelIncreasment_CreatingNewRogueCharacterWithLevelOne_ShouldReturnStrengthThreeDexterityTenIntelliganceTwoLevelTwo()
        {
            //Arrange
            Rogue rogue2 = new Rogue();
            int[] expected = new int[] { 3, 10, 2, 2 };

            //Act
            rogue2.LevelIncreasment();

            //Assert
            Assert.Equal(expected, new int[] { rogue2.PrimaryAttributes.Strength, rogue2.PrimaryAttributes.Dexterity, rogue2.PrimaryAttributes.Intelligence, rogue2.Level });
        }
    }
}
