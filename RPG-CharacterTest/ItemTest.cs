﻿using System;
using Xunit;
using RPG_Characters;

namespace RPG_CharacterTest
{
    public class ItemTest
    {
        [Fact]
        public void EquipWeapon_CheckIfTheWarriorLevelIsEnoughToEquipTheWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons() { Name = "Common axe", RequiredLevel = 2, ItemSlot = Slot.Weapon, WeaponsType = Weapons.CharWeaponsType.Axe, WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 } };
            string expected = "Sorry! You can not equip this weapon.";

            //Act
            InvalidWeaponException actual = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipArmor_CheckIfTheWarriorLevelIsEnoughtoEquipTheArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Armor testPlateBody = new Armor() { Name = "Common plate body armor", RequiredLevel = 2, ItemSlot = Slot.Body, ArmorsType = Armor.CharArmorsType.Plate, Attributes = new PrimaryAttributes() { Strength = 1 } };
            string expected = "Sorry! You can not equip this armor.";

            //Act
            InvalidArmorException actual = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipWeapon_CheckIfTheWeaponWithTypeBowsIsAvailableForWarriorCharacter_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testBow = new Weapons() { Name = "Common bow", RequiredLevel = 1, ItemSlot = Slot.Weapon, WeaponsType = Weapons.CharWeaponsType.Bow, WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 } };
            string expected = "Sorry! You can not equip this weapon.";

            //Act
            InvalidWeaponException actual = Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipArmor_CheckIfTheArmorWithTypeClothIsAvailableForWarriorCharacter_ShouldReturnSuccessfulMessage()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Armor testClothHead = new Armor() { Name = "Common cloth head armor", RequiredLevel = 1, ItemSlot = Slot.Head, ArmorsType = Armor.CharArmorsType.Cloth, Attributes = new PrimaryAttributes() { Intelligence = 5 } };
            string expected = "Sorry! You can not equip this armor.";

            //Act
            InvalidArmorException actual = Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothHead));

            //Assert
            Assert.Equal(expected, actual.Message);
        }

        [Fact]
        public void EquipWeapon_EquipAValidWeaponForWarriorCharacter_ShouldReturnASuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons() { Name = "Common axe", RequiredLevel = 1, ItemSlot = Slot.Weapon, WeaponsType = Weapons.CharWeaponsType.Axe, WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 } };
            string expected = "New weapon equipped!";

            //Act
            string actual = warrior.EquipWeapon(testAxe);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_EquipAValidArmorForWarriorCharacter_ShouldReturnASuccessMessage()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Armor testPlateBody = new Armor() { Name = "Common plate body armor", RequiredLevel = 1, ItemSlot = Slot.Body, ArmorsType = Armor.CharArmorsType.Plate, Attributes = new PrimaryAttributes() { Strength = 1 } };
            string expected = "New armor equipped!";

            //Act
            string actual = warrior.EquipArmor(testPlateBody);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterIfNoWeaponIsEquipped_ShouldReturnOneAndFiveHundredths()
        {
            //Arrange
            Warrior warrior = new Warrior();
            double expected = 1 * (1 + (5 * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterWithAxeWeaponEquipped_ShouldReturnEightAndEightyFiveThousand()
        {
            //Arrange
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons() { Name = "Common axe", RequiredLevel = 1, ItemSlot = Slot.Weapon, WeaponsType = Weapons.CharWeaponsType.Axe, WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 } };
            warrior.EquipWeapon(testAxe);
            double expected = (7 * 1.1) * (1 + (5 * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_CalculateTheDamageOfWarriorCharacterWithAxeWeaponAndPlateBodyArmorEquipped_ShouldReturnEightPointOneHundredSixtyTwo()
        {
            //Arranges
            Warrior warrior = new Warrior();
            Weapons testAxe = new Weapons() { Name = "Common axe", RequiredLevel = 1, ItemSlot = Slot.Weapon, WeaponsType = Weapons.CharWeaponsType.Axe, WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 } };
            Armor testPlateBody = new Armor() { Name = "Common plate body armor", RequiredLevel = 1, ItemSlot = Slot.Body, ArmorsType = Armor.CharArmorsType.Plate, Attributes = new PrimaryAttributes() { Strength = 1 } };
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody);
            double expected = (7 * 1.1) * (1 + ((5 + 1) * 0.01));

            //Act
            double actual = warrior.Damage();

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
