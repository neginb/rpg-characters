﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Warrior : Character
    {
        //Constructor - Making a new Warrior character
        public Warrior()
        {
            //Get access to priymay attributes class and set the values
            PrimaryAttributes = new PrimaryAttributes
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponsType == Weapons.CharWeaponsType.Axe && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel || weapon.WeaponsType == Weapons.CharWeaponsType.Hammer && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel || weapon.WeaponsType == Weapons.CharWeaponsType.Sword && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel)
                {
                    Equipment.Add(weapon.ItemSlot, weapon); 
                    return "New weapon equipped!";
            }
            else
                {                    
                    throw new InvalidWeaponException();
                }
        }

        public override string EquipArmor(Armor armor)
        {
                if (armor.ArmorsType == Armor.CharArmorsType.Mail && armor.ItemSlot != Slot.Weapon && Level >= armor.RequiredLevel || armor.ArmorsType == Armor.CharArmorsType.Plate && armor.ItemSlot != Slot.Weapon && Level >= armor.RequiredLevel)
                {
                    Equipment.Add(armor.ItemSlot, armor);
                    return "New armor equipped!";
                
                }
                else
                {
                    throw new InvalidArmorException();
                }
        }

        //Primary attribute Strength is the Warriors' primary attribute which increase the damage of this character by one procent
        public override double TotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor gainedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += gainedArmor.Attributes.Strength;
                }
            }
            TotalPrimaryAttributes += PrimaryAttributes.Strength;
            return TotalPrimaryAttributes;
        }

        //This method increase the value of Warriors' primary attributes
        //Increase Strength by 3, Dexterity by 2, Intelligence by 1 and the level by 1
        public override void LevelIncreasment()
        {
            PrimaryAttributes.Strength += 3;
            PrimaryAttributes.Dexterity += 2;
            PrimaryAttributes.Intelligence += 1;
            Level += 1;
        }
    }
}
