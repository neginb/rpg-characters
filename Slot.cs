﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    //Using enum to give the available types to the Slot

    public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon
        }
    }
