﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    [Serializable]
    public class InvalidWeaponException : Exception
  {
        public override string Message
        {
            get
            {
                //return this message if an InvalidWeaponException is thrown
                return "Sorry! You can not equip this weapon.";
            }
        }
    }
}

