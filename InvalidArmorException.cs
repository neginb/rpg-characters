﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public override string Message
        {
            get
            {
                //return this message if an InvalidArmorException is thrown
                return "Sorry! You can not equip this armor.";
            }
        }
    }
}
