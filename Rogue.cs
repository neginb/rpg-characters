﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class Rogue : Character
    {
        //Constructor - Making a new Rogue character
        public Rogue()
        {
            //Get access to priymay attributes class and set the values
            PrimaryAttributes = new PrimaryAttributes
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1
            };
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponsType == Weapons.CharWeaponsType.Dagger && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel || weapon.WeaponsType == Weapons.CharWeaponsType.Sword && weapon.ItemSlot == Slot.Weapon && Level >= weapon.RequiredLevel)
                {
                    Equipment.Add(weapon.ItemSlot, weapon);
                    return "New weapon equipped!";
            }
            else
                {
                    throw new InvalidWeaponException();
                }
        }

        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmorsType == Armor.CharArmorsType.Mail && armor.ItemSlot != Slot.Weapon && Level >= armor.RequiredLevel || armor.ArmorsType == Armor.CharArmorsType.Leather && armor.ItemSlot != Slot.Weapon && Level >= armor.RequiredLevel)
                {
                    Equipment.Add(armor.ItemSlot, armor);
                    return "New armor equipped!";   
            }
            else
                {
                    throw new InvalidArmorException();
                }
        }

        //Primary attribute Dexterity is the Rogues' primary attribute which increase the damage of this character by one procent
        public override double TotalAttributes()
        {
            TotalPrimaryAttributes = 0;
            foreach (var item in Equipment)
            {
                if (item.Key != Slot.Weapon)
                {
                    Armor gainedArmor = (Armor)Equipment[item.Key];
                    TotalPrimaryAttributes += gainedArmor.Attributes.Dexterity;
                }
            }
            TotalPrimaryAttributes += PrimaryAttributes.Dexterity;
            return TotalPrimaryAttributes;
        }

        public override void LevelIncreasment()
        {
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 4;
            PrimaryAttributes.Intelligence += 1;
            Level += 1;
        }
    }
}
